/*
 Put header here
 */
package com.example.SSHW;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

@ComponentScan(basePackages = "com.example.SSHW")
public class Application  {


    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        
        Application p  = context.getBean(Application.class);
        
        p.start();
        
        
    }
    @Autowired
    private Message message;
    private void start() {
    	System.out.println("Message : "+message.getMessage());
    }
}
