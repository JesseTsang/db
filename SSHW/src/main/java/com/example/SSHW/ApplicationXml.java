package com.example.SSHW;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class ApplicationXml {
    
   public static void main(String[] args) {
       
      XmlBeanFactory factory = 
             new XmlBeanFactory(new ClassPathResource("my-beans.xml"));

      MessageXML obj = (MessageXML) factory.getBean("mymessage");

      String msg = obj.getMessage();
      System.out.println(msg);
   }
}