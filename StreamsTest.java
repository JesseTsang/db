import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;

public class StreamsTest{

    public static void main(String[] args) {
        System.out.println("Using old method");

        List<String> strings = Arrays.asList("abc","", "def","", "ghi", "abdsf", "cfgjs");
        System.out.println("List: "+strings);
        
        List<String> filteredList = strings.stream().
                                                filter(string ->!string.isEmpty()).
                                            collect(Collectors.toList());
        System.out.println("Filtered List using java 8 streams and lambda: "+filteredList);
    }

    
}