import java.sql.*;
public class FirstExample {
   static final String DB_URL = "jdbc:mysql://localhost/testDB";
   static final String USER = "root";
   static final String PASS = "RedHat@123";
   static final String QUERY = "SELECT boy_id, boy, toy_id FROM boys";

   public static void main(String[] args) {
      // Open a connection
      try(Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
         Statement stmt = conn.createStatement();
         ResultSet rs = stmt.executeQuery(QUERY);) {
         // Extract data from result set
         while (rs.next()) {
            // Retrieve by column name
            System.out.print("ID: " + rs.getInt("boy_id"));
            System.out.print(", Name: " + rs.getString("boy"));
            System.out.print(", Toy ID: " + rs.getInt("toy_id"));
         }
      } catch (SQLException e) {
         e.printStackTrace();
      } 
   }
}
